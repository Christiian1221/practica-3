package mx.unitec.moviles.practica3


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_auto_text.*


class AutoTextActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auto_text)

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.alcaldia_array,
            android.R.layout.select_dialog_item
        )

        autoCompleteTextView2.threshold = 2
        autoCompleteTextView2.setAdapter(adapter)
        autoCompleteTextView2.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText( this,
                position.toString()+" : "+parent?.getItemIdAtPosition(position).toString(),
                Toast.LENGTH_SHORT).show()


        }
    }
}