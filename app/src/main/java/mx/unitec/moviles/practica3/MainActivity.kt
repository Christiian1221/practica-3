package mx.unitec.moviles.practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var autoTextView: AutoCompleteTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        autoTextView = findViewById(R.id.autoCompleteTextView2)
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.alcaldia_array,
            android.R.layout.select_dialog_item
        )
        autoTextView.threshold = 2

        autoTextView.setAdapter(adapter)


        autoTextView.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this, adapter.getItem(position), Toast.LENGTH_SHORT).show()
        }



    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item?.itemId) {
            R.id.new_option -> {
                Toast.makeText(this, "New Option Selected", Toast.LENGTH_SHORT).show()
                true
            }

            R.id.help -> {
                Toast.makeText(this, "Help Selected", Toast.LENGTH_SHORT).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }


}