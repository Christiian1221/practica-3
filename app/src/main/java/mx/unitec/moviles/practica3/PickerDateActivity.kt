package mx.unitec.moviles.practica3

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.aplicaciontres.ui.DatePickerFragment
import kotlinx.android.synthetic.main.activity_picker_date.*

class PickerDateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker_date)
    }

    fun showDatePickerDialog(v: View){
        //val timerPickerFragment = TimePickerFragment()

        val datePickerFragment = DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener
        { view, year, month, dayOfMonth -
            pkrDate.setText("${year}-${month}-${dayOfMonth}")
        })

        datePickerFragment.show(supportFragmentManager, "datePicker")>

    }
}
