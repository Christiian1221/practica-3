package mx.unitec.moviles.practica3

import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.aplicaciontres.ui.TimePickerFragment
import kotlinx.android.synthetic.main.activity_picker.*
import mx.unitec.moviles.practica3.ui.TimePickerFragment

class PickerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)
    }

    fun showTimePickerDialog(v:View){
        //val timerPickerFragment = TimePickerFragment()

        val timerPickerFragment = TimePickerFragment.newInstance(TimePickerDialog.OnTimeSetListener{
                view, hourOfDay, minute ->
            pkrtime.setText("${hourOfDay}:${minute}")
        })
        timerPickerFragment.show(supportFragmentManager, "timePicker")
    }
}